"use strict"


document.addEventListener('DOMContentLoaded', function() {
    const tabsContainer = document.querySelector('.tabs');
    const tabContents = document.querySelectorAll('.tab-content');

    tabsContainer.addEventListener('click', function(event) {
        if(event.target.classList.contains('tabs-title')) {
            const tabId = event.target.getAttribute('data-tab');

            tabContents.forEach(content => {
                content.classList.remove('active');
            });

            const activeTab = document.getElementById(tabId);
            activeTab.classList.add('active');

            tabsContainer.querySelectorAll('.tabs-title').forEach(tab => {
                tab.classList.remove('active');
            });

            event.target.classList.add('active');
        }
    });
});